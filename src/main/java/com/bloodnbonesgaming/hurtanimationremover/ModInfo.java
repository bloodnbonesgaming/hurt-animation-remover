package com.bloodnbonesgaming.hurtanimationremover;

public class ModInfo {
	public static final String MODID = "hurtanimationremover";
	public static final String MOD_NAME = "Hurt Animation Remover";
	public static final String VERSION = "@VERSION@";
}
