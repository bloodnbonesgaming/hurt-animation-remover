package com.bloodnbonesgaming.hurtanimationremover;

import org.apache.logging.log4j.Logger;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = ModInfo.MODID, name = ModInfo.MOD_NAME, version = ModInfo.VERSION, clientSideOnly = true, dependencies = "required-after:bnbgamingcore@[0.11.0,);", acceptedMinecraftVersions = "[1.12,1.13)")
public class HurtAnimationRemover {
	
	@Instance(ModInfo.MODID)
	public static HurtAnimationRemover instance;
		
	private Logger log;
	
	@EventHandler
	public void preInit(final FMLPreInitializationEvent event)
	{
		this.log = event.getModLog();
	}

	public Logger getLog() {
		return this.log;
	}
}
